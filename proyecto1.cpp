#include<bits/stdc++.h>


using namespace std;
# define INF 0x3f3f3f3f
 
// grafo armado mediante listas de adyacencia
class Graph
{   // numero de vértices
    int V;
 
    // pair usado para almacenar los dos valores juntos (en este caso dos int)
    list< pair<int, int> > *adj;
 
public:
    Graph(int V);  // Constructor

    void addEdge(int u, int v, int w);
    // llamada a dijkstra
    void dijkstraAlgorithm(int s);
};
 

Graph::Graph(int V)
{
    this->V = V;
    adj = new list< pair<int, int> >[V];
}
// add edge agreda el peso en los nodos 
void Graph::addEdge(int u, int v, int w)
{       
    
    adj[u].push_back(make_pair(v, w));
    adj[v].push_back(make_pair(u, w));
}
 
// Camino más corto de todos los vértices 
void Graph::dijkstraAlgorithm(int src)
{
    // Create a set to store vertices that are being
    // processed
    set< pair<int, int> > setds;
 
    // Create a vector for distances and initialize all
    // distances as infinite (INF)
    //se almacenan todas las mínimas distancias entre nodos  
    vector<int> dist(V, INF);
 
    // Insert source itself in Set and initialize its
    // distance as 0.
    setds.insert(make_pair(0, src));
    dist[src] = 0;
 
    /* Looping till all shortest distance are finalized
       then setds will become empty    */
    while (!setds.empty())
    {
        // The first vertex in Set is the minimum distance
        // vertex, extract it from set.
        pair<int, int> tmp = *(setds.begin());
        setds.erase(setds.begin());
 
        // vertex label is stored in second of pair (it
        // has to be done this way to keep the vertices
        // sorted distan ce (distance must be first item
        // in pair)
        int u = tmp.second;
 
        // 'i' is used to get all adjacent vertices of a vertex
        list< pair<int, int> >::iterator i;
        for (i = adj[u].begin(); i != adj[u].end(); ++i)
        {
            // Get vertex label and weight of current adjacent
            // of u.
            int v = (*i).first;
            int weight = (*i).second;
 
            //    If there is shorter path to v through u.
            if (dist[v] > dist[u] + weight)
            {

                if (dist[v] != INF)
                    setds.erase(setds.find(make_pair(dist[v], v)));
 
                // actualizar distancia de v
                dist[v] = dist[u] + weight;
                setds.insert(make_pair(dist[v], v));
            }
        }
    }
 
    //menor distancia desde origen elegido 
    printf("Vertex   Distance from Source\n");
    for (int i = 0; i < V; ++i)
        printf("%d \t\t %d\n", i, dist[i]);
}
 
// Driver program to test methods of graph class
int main()
{
    // representación del grafo "neurona" 
    int V = 11;
    Graph g(V);
 
    // vertice, unión con vertice "x" y peso o distancia a ese vértice 
    
    g.addEdge(0, 4, 1);
    g.addEdge(1, 4, 8);
    g.addEdge(2, 4, 3);
    g.addEdge(3, 4, 13);
    g.addEdge(4, 5, 1);
    g.addEdge(5, 6, 2);
    g.addEdge(5, 7, 6);
    g.addEdge(7, 8, 1);
    g.addEdge(8, 9, 4);
    g.addEdge(8, 10, 1);
    
    g.dijkstraAlgorithm(4);
 
    return 0;
}